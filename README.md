# Clean Code

For every chapter that exists in [Clean Code Book](https://learning.oreilly.com/library/view/clean-code-a/9780136083238/) <sup>[Get Paid Access](https://gitlab.com/smarter-codes/guidelines/about-guidelines/-/blob/master/paid-learning-material.md)</sup>, we create an issue here.

Why? During the [code reviews](../../code-review/) we cite the issues in here inside the Merge Requests as
every chapter in this book acts as a checklist for 'clean code'
